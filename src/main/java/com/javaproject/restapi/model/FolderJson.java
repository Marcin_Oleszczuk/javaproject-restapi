package com.javaproject.restapi.model;

public class FolderJson {
    private String id;
    private Object assets;

    public FolderJson(String id, Object assets) {
        this.id = id;
        this.assets = assets;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getAssets() {
        return assets;
    }

    public void setAssets(Object assets) {
        this.assets = assets;
    }
}
