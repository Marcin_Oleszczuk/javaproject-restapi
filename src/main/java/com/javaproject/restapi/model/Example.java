
package com.javaproject.restapi.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "//test-path/CloudUX1/proj1/proj1 Bin.avb"
})
public class Example {

    @JsonProperty("//test-path/CloudUX1/proj1/proj1 Bin.avb")
    private FolderFromJSON folderFromJSON;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("//test-path/CloudUX1/proj1/proj1 Bin.avb")
    public FolderFromJSON getFolderFromJSON() {
        return folderFromJSON;
    }

    @JsonProperty("//test-path/CloudUX1/proj1/proj1 Bin.avb")
    public void setFolderFromJSON(FolderFromJSON folderFromJSON) {
        this.folderFromJSON = folderFromJSON;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
