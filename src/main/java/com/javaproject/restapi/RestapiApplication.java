package com.javaproject.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaproject.restapi.model.FolderFromJSON;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;

import java.io.*;

@SpringBootApplication
public class RestapiApplication{
    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
    @Bean("FullDescription")
    public FolderFromJSON JSONFile() throws IOException {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("files/data.json");
        String jsondata = readFromInputStream(inputStream);
        //read json file and convert to customer object
        FolderFromJSON data = objectMapper.readValue(jsondata,FolderFromJSON.class);
//        FolderFromJSON data = objectMapper.readValue(new File("C:\\Users\\om39385\\IdeaProjects\\restapi\\src\\main\\resources\\files\\data.json"), FolderFromJSON.class);
        return data;
    }
    public static void main(String[] args){
        SpringApplication.run(RestapiApplication.class, args);

    }

}
