
package com.javaproject.restapi.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "essenceType",
    "track",
    "start",
    "length",
    "mobId",
    "online",
    "type"
})
public class MediaItem {

    @JsonProperty("essenceType")
    private String essenceType;
    @JsonProperty("track")
    private String track;
    @JsonProperty("start")
    private String start;
    @JsonProperty("length")
    private String length;
    @JsonProperty("mobId")
    private String mobId;
    @JsonProperty("online")
    private String online;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("essenceType")
    public String getEssenceType() {
        return essenceType;
    }

    @JsonProperty("essenceType")
    public void setEssenceType(String essenceType) {
        this.essenceType = essenceType;
    }

    @JsonProperty("track")
    public String getTrack() {
        return track;
    }

    @JsonProperty("track")
    public void setTrack(String track) {
        this.track = track;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    @JsonProperty("length")
    public String getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(String length) {
        this.length = length;
    }

    @JsonProperty("mobId")
    public String getMobId() {
        return mobId;
    }

    @JsonProperty("mobId")
    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    @JsonProperty("online")
    public String getOnline() {
        return online;
    }

    @JsonProperty("online")
    public void setOnline(String online) {
        this.online = online;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
