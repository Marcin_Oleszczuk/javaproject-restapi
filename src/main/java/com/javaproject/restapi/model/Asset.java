
package com.javaproject.restapi.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "attributes",
    "base",
    "mobId",
    "common",
    "media-items"
})
public class Asset {

    @JsonProperty("attributes")
    private List<Attribute> attributes = null;
    @JsonProperty("base")
    private Base base;
    @JsonProperty("mobId")
    private String mobId;
    @JsonProperty("common")
    private Common common;
    @JsonProperty("media-items")
    private List<MediaItem> mediaItems = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("attributes")
    public List<Attribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("base")
    public Base getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(Base base) {
        this.base = base;
    }

    @JsonProperty("mobId")
    public String getMobId() {
        return mobId;
    }

    @JsonProperty("mobId")
    public void setMobId(String mobId) {
        this.mobId = mobId;
    }

    @JsonProperty("common")
    public Common getCommon() {
        return common;
    }

    @JsonProperty("common")
    public void setCommon(Common common) {
        this.common = common;
    }

    @JsonProperty("media-items")
    public List<MediaItem> getMediaItems() {
        return mediaItems;
    }

    @JsonProperty("media-items")
    public void setMediaItems(List<MediaItem> mediaItems) {
        this.mediaItems = mediaItems;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
