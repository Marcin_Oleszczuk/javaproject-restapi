#Instruction(How to run it):
1. Clone from gitlab repository
2. Run application
3. Go to http://localhost:3000

#Instruction Docker:
1. Type in terminal ``` docker build -t my_backend . ```
2. Type in terminal ``` docker run -p 8085:8085 my_backend ```
3. Go to http://localhost:3001

**WARNIRNG**
Remember under localhost:3001 [frontend](https://gitlab.com/Marcin_Oleszczuk/restapi_frontend) must be available

