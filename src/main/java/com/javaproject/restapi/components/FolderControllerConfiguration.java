package com.javaproject.restapi.components;

import com.javaproject.restapi.model.FolderFromJSON;
import com.javaproject.restapi.model.FolderJson;
import com.javaproject.restapi.model.Result;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.*;

@Configuration
public class FolderControllerConfiguration {
    private final FolderFromJSON Folder;

    public FolderControllerConfiguration(FolderFromJSON Folder) {
        this.Folder = Folder;
    }

    @Bean("RootResult")
    public Map<String, List<Result>> RootGetResult() {
        Map<String, List<Result>> mapResult = new HashMap<String, List<Result>>();
        List<Result> resultList = new ArrayList<>();

        Folder.getAdditionalProperties().forEach((key, value)-> {
            LinkedHashMap<String, FolderJson> castMap = (LinkedHashMap<String, FolderJson>)value;
            Collection<FolderJson> idCollection = castMap.values();
            Result result = new Result((String)idCollection.toArray()[0], key);
            resultList.add(result);
        });
        mapResult.put("result", resultList);

        return mapResult;
    }
}
