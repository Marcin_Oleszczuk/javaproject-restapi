FROM openjdk:8
ADD /build/libs/restapi-0.0.1-SNAPSHOT.jar restapi-0.0.1-SNAPSHOT.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "restapi-0.0.1-SNAPSHOT.jar"]