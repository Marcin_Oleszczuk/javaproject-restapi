package com.javaproject.restapi.controller;

import com.javaproject.restapi.exceptions.ResourceNotFoundException;
import com.javaproject.restapi.model.FolderFromJSON;
import com.javaproject.restapi.model.FolderJson;
import com.javaproject.restapi.model.Result;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

@RestController
public class FoldersRootController {
    private final FolderFromJSON Folder;
    @Resource(name = "RootResult")
    private Map map;

    public FoldersRootController(FolderFromJSON Folder) {
        this.Folder = Folder;
    }

    @RequestMapping("/")
    public ResponseEntity Root(@RequestParam(defaultValue = "0") int skip, @RequestParam(defaultValue = "0") int limit) {
        ArrayList<Result> itemList = (ArrayList<Result>) map.get("result");
        int end = 0;
        if ((skip | limit) < 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        if (skip >= itemList.size()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        if (limit >= itemList.size()) {
            end = itemList.size();
        }
        if (skip + limit < itemList.size()) {
            end = skip + limit;
        } else if (skip + limit >= itemList.size()) {
            end = itemList.size();
        }
        if (limit == 0) {
            end = itemList.size();
        }

        List<Result> skipList = itemList.subList(skip, end);
        Map<String, List<Result>> arrayListMap = new HashMap<>();
        arrayListMap.put("result",skipList);

        return ResponseEntity.status(HttpStatus.OK).body(arrayListMap);
    }
    @RequestMapping("/{folderId}")
    public LinkedHashMap<String, FolderJson> FolderDetails(@PathVariable String folderId) {
        for (Map.Entry<String, Object> entry : Folder.getAdditionalProperties().entrySet()) {
            Object value = entry.getValue();
            LinkedHashMap<String, FolderJson> castMap = (LinkedHashMap<String, FolderJson>) value;
            Collection<FolderJson> idCollection = castMap.values();
            String foundId = (String) idCollection.toArray()[0];
            if (foundId.equals(folderId)) {
                return castMap;
            }
        }
        throw new ResourceNotFoundException("Folder with provided ID not found");
    }
}
